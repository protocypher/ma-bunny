# Bunny

**Doctor Scheduler**

Bunny is a basic, text based, doctor scheduling application that uses the
`cmd.Cmd` framework. It manages appointments with patients and doctors. It
allows patients to set hooks so that if a doctor cancels an appointment in
a slot they more prefer it notifies the agent so they can immediately call
the patient.

