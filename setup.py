from setuptools import setup


setup(
      name="Bunny",
      version="0.1.0",
      packages=["bunny"],
      url="https://bitbucket.org/protocypher/ma-bunny",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A doctor schedule management application."
)

